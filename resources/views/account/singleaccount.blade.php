@extends('layouts.app')
@section('content')
<div class="container">
            <h1>Single Account Payments Data {{$account->name}}  </h1>

            <table class="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">Subject</th>
                    <th scope="col">Account ID</th>
                    <th scope="col">PaymentDate</th>
                    <th scope="col">Payment Pending</th>
                    <th scope="col">Payment Recived</th>
                    <th scope="col">Pending amount</th>
                  </tr>
                </thead>
            @foreach ($account->payments as $payment)
            <tbody>
              <tr>
               <td> {{$payment->subject}}</td>
               <td> {{$payment->account_id}}</td>
               <td> {{$payment->payment_date}}</td>
               <td> {{$payment->payment_pending}}</td>
               <td> {{$payment->payment_recived}}</td>
               <td> {{$payment->pending_amount}}</td>
            </tr>
            
        </tbody>
        
        @endforeach
    </table>
        </div>
@endsection