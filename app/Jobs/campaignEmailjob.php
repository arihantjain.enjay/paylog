<?php

namespace App\Jobs;

use App\Account;
use App\campaignevent;
use App\Mail\campaign;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class campaignEmailjob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $allaccountemails;
    public $body;
    public function __construct($allaccountemails, $body)
    {
        // dd($allaccountemails);
        $this->allaccountemails = $allaccountemails;
        $this->body = $body;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // dd($this->allaccountemails);
        dd($this->allaccountemails , "here");
        foreach ($this->allaccountemails as $accountmail) {
            event(new  campaignevent($accountmail, $this->body));
        }
    }
}
